package com.larkin.tinkoffnews.base;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface BaseView {
    void showProgress(boolean show);
    void showError(Throwable e);
}
