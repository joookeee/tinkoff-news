package com.larkin.tinkoffnews.base;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface BasePresenter<T extends BaseView>{
    void bind(T view);
    void unbind();
}
