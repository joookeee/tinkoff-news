package com.larkin.tinkoffnews.content.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class CustomDate {
    @SerializedName("milliseconds")
    private long date;

    public long getDate() {
        return date;
    }
}
