package com.larkin.tinkoffnews.content.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class ContentPayload {
    @SerializedName("title")
    private NewsPayload title;

    @SerializedName("creationDate")
    private CustomDate creationDate;

    @SerializedName("lastModificationDate")
    private CustomDate lastModificationDate;

    @SerializedName("content")
    private String content;

    @SerializedName("bankInfoTypeId")
    private int bankInfoTypeId;

    @SerializedName("typeId")
    private String typeId;

    public NewsPayload getTitle() {
        return title;
    }

    public CustomDate getCreationDate() {
        return creationDate;
    }

    public CustomDate getLastModificationDate() {
        return lastModificationDate;
    }

    public String getContent() {
        return content;
    }

    public int getBankInfoTypeId() {
        return bankInfoTypeId;
    }

    public String getTypeId() {
        return typeId;
    }
}
