package com.larkin.tinkoffnews.content.repository;

import com.larkin.tinkoffnews.content.model.Content;
import com.larkin.tinkoffnews.content.model.News;

import rx.Observable;


/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NewsRepository {
    Observable<News> getNews();
    Observable<Content> getNewsDetails(int id);
}
