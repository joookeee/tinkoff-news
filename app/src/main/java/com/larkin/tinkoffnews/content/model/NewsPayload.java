package com.larkin.tinkoffnews.content.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsPayload implements Comparable<NewsPayload> {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("text")
    private String text;

    @SerializedName("publicationDate")
    private CustomDate publicationDate;

    @SerializedName("bankInfoTypeId")
    private int bankInfoTypeId;

    @Override
    public int compareTo(@NonNull NewsPayload newsPayload) {
        return this.publicationDate.getDate() < newsPayload.publicationDate.getDate() ? 1 : this.publicationDate.getDate() > newsPayload.publicationDate.getDate() ? 0 : 1;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public CustomDate getPublicationDate() {
        return publicationDate;
    }

    public int getBankInfoTypeId() {
        return bankInfoTypeId;
    }
}
