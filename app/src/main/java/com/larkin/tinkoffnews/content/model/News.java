package com.larkin.tinkoffnews.content.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class News {
    @SerializedName("resultCode")
    private String resultCode;

    @SerializedName("payload")
    private List<NewsPayload> payload;

    public String getResultCode() {
        return resultCode;
    }

    public List<NewsPayload> getPayload() {
        return payload;
    }
}
