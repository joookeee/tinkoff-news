package com.larkin.tinkoffnews.content.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class Content {
    @SerializedName("resultCode")
    private String resultCode;

    @SerializedName("payload")
    private ContentPayload payload;

    public String getResultCode() {
        return resultCode;
    }

    public ContentPayload getPayload() {
        return payload;
    }
}
