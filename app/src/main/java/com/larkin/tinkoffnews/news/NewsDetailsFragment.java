package com.larkin.tinkoffnews.news;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.larkin.tinkoffnews.R;
import com.larkin.tinkoffnews.TinkoffNewsApplication;
import com.larkin.tinkoffnews.content.model.Content;
import com.larkin.tinkoffnews.news.interfaces.NewsDetailsPresenter;
import com.larkin.tinkoffnews.news.interfaces.NewsDetailsView;
import com.larkin.tinkoffnews.utils.NetworkConnectionException;

import javax.inject.Inject;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsDetailsFragment extends Fragment implements NewsDetailsView{

    private static final String CONTENT_ID = "content_id";

    @Inject
    NewsDetailsPresenter presenter;
    private TextView newsDetails;
    private Button refreshButton;
    private int newsId;

    public static NewsDetailsFragment newInstance(int id) {
        NewsDetailsFragment fragment = new NewsDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(CONTENT_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TinkoffNewsApplication.getIocComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_news_details, container, false);
        newsId = getArguments().getInt(CONTENT_ID);
        newsDetails = (TextView) view.findViewById(R.id.news_details);
        refreshButton = (Button) view.findViewById(R.id.refresh);
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.news_details_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.bind(this);
        presenter.refreshNewsDetails(newsId);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unbind();
    }

    @Override
    public void showNewsDetails(Content content) {
        newsDetails.setText(Html.fromHtml(content.getPayload().getContent()));
    }

    @Override
    public void showProgress(boolean show) {

    }

    @Override
    public void showError(Throwable e) {
        if (e instanceof NetworkConnectionException) {
            Toast.makeText(getContext(), R.string.err_connection, Toast.LENGTH_SHORT).show();
            refreshButton.setVisibility(View.VISIBLE);
        } else {
            Log.w(NewsDetailsFragment.class.getName(), e.getMessage());
        }
    }
}
