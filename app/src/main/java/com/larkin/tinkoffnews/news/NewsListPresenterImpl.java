package com.larkin.tinkoffnews.news;

import com.larkin.tinkoffnews.content.model.News;
import com.larkin.tinkoffnews.content.repository.NewsRepository;
import com.larkin.tinkoffnews.network.NetworkManager;
import com.larkin.tinkoffnews.news.interfaces.NewsListPresenter;
import com.larkin.tinkoffnews.news.interfaces.NewsListView;
import com.larkin.tinkoffnews.utils.NetworkConnectionException;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsListPresenterImpl implements NewsListPresenter {

    private NewsListView view;
    private final CompositeSubscription subscription;
    private final NewsRepository repository;
    private final NetworkManager networkManager;

    public NewsListPresenterImpl(NewsRepository repository, NetworkManager networkManager) {
        subscription = new CompositeSubscription();
        this.repository = repository;
        this.networkManager = networkManager;
    }

    @Override
    public void refreshNews() {
        if (!networkManager.isNetworkAvailable()) {
            onError.call(new NetworkConnectionException());
        }
        view.showProgress(true);
        subscription.add(repository.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext, onError));
    }

    private final Action1<News> onNext = news -> {
        view.showProgress(false);
        view.showNews(news);
    };

    private final Action1<Throwable> onError = e -> {
        view.showProgress(false);
        view.showError(e);
    };

    @Override
    public void bind(NewsListView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }
}
