package com.larkin.tinkoffnews.news.interfaces;

import com.larkin.tinkoffnews.base.BaseView;
import com.larkin.tinkoffnews.content.model.News;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NewsListView extends BaseView{
    void showNews(News news);
}
