package com.larkin.tinkoffnews.news;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.larkin.tinkoffnews.R;
import com.larkin.tinkoffnews.TinkoffNewsApplication;
import com.larkin.tinkoffnews.content.model.News;
import com.larkin.tinkoffnews.news.interfaces.NewsListPresenter;
import com.larkin.tinkoffnews.news.interfaces.NewsListView;
import com.larkin.tinkoffnews.utils.NetworkConnectionException;
import com.larkin.tinkoffnews.utils.interfaces.NewsNavigationController;

import javax.inject.Inject;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsListFragment extends Fragment implements NewsListView, SwipeRefreshLayout.OnRefreshListener, NewsListAdapter.OnItemClickListener {

    @Inject
    NewsListPresenter presenter;

    NewsNavigationController navController;

    private NewsListAdapter newsListAdapter;

    private SwipeRefreshLayout layout;
    private RecyclerView recyclerView;
    private TextView emptyView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NewsNavigationController) {
            navController = (NewsNavigationController) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TinkoffNewsApplication.getIocComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        layout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);
        recyclerView = (RecyclerView) view.findViewById(R.id.news_list);
        emptyView = (TextView)view.findViewById(R.id.news_empty_view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newsListAdapter = new NewsListAdapter();
        newsListAdapter.setOnItemClickListener(this);

        recyclerView.setAdapter(newsListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        layout.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.bind(this);
        presenter.refreshNews();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unbind();
    }

    @Override
    public void onRefresh() {
        presenter.refreshNews();
    }

    @Override
    public void onItemClick(int id) {
        navController.openNews(id);
    }

    @Override
    public void showNews(News news) {
        if (news != null) newsListAdapter.swap(news.getPayload());
        emptyView.setVisibility(newsListAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProgress(boolean show) {
        layout.setRefreshing(show);
    }

    @Override
    public void showError(Throwable e) {
        if (e instanceof NetworkConnectionException) {
            Toast.makeText(getContext(), R.string.err_connection, Toast.LENGTH_SHORT).show();
        } else {
            Log.w(NewsListFragment.class.getName(), e.getMessage());
        }
    }
}
