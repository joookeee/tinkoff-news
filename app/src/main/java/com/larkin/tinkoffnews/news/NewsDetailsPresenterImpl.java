package com.larkin.tinkoffnews.news;

import com.larkin.tinkoffnews.content.model.Content;
import com.larkin.tinkoffnews.content.repository.NewsRepository;
import com.larkin.tinkoffnews.network.NetworkManager;
import com.larkin.tinkoffnews.news.interfaces.NewsDetailsPresenter;
import com.larkin.tinkoffnews.news.interfaces.NewsDetailsView;
import com.larkin.tinkoffnews.utils.NetworkConnectionException;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsDetailsPresenterImpl implements NewsDetailsPresenter {

    private NewsDetailsView view;
    private final CompositeSubscription subscription;
    private final NewsRepository repository;
    private final NetworkManager networkManager;

    public NewsDetailsPresenterImpl(NewsRepository repository, NetworkManager networkManager) {
        subscription = new CompositeSubscription();
        this.repository = repository;
        this.networkManager = networkManager;
    }

    @Override
    public void refreshNewsDetails(int id) {
        if (!networkManager.isNetworkAvailable()) {
            onError.call(new NetworkConnectionException());
        } else {
            subscription.add(repository.getNewsDetails(id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(onNext, onError));
        }
    }

    @Override
    public void bind(NewsDetailsView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }

    private final Action1<Content> onNext = content -> {
        view.showProgress(false);
        view.showNewsDetails(content);
    };

    private final Action1<Throwable> onError = e -> {
        view.showProgress(false);
        view.showError(e);
    };
}
