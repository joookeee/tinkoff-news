package com.larkin.tinkoffnews.news.interfaces;

import com.larkin.tinkoffnews.base.BasePresenter;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NewsDetailsPresenter extends BasePresenter<NewsDetailsView> {
    void refreshNewsDetails(int id);
}
