package com.larkin.tinkoffnews.news.cache;

import android.util.Log;

import com.larkin.tinkoffnews.source.local.FileManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class FileManagerImpl implements FileManager {
    @Override
    public void writeToFile(File file, String content) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            writer.write(content);
        } catch (IOException e) {
            Log.e(FileManagerImpl.class.getName(), e.getMessage());
        } finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException e) {
                    Log.e(FileManagerImpl.class.getName(), e.getMessage());
                }
            }
        }
    }

    @Override
    public String readFromFile(File file) {
        StringBuilder fileContentBuilder = new StringBuilder();
        if (file.exists()) {
            String stringLine;
            FileReader fileReader = null;
            try {
                fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                while ((stringLine = bufferedReader.readLine()) != null) {
                    fileContentBuilder.append(stringLine).append("\n");
                }
                bufferedReader.close();

            } catch (IOException e) {
                Log.e(FileManagerImpl.class.getName(), e.getMessage());
            } finally {
                if (fileReader != null) {
                    try {
                        fileReader.close();
                    } catch (IOException e) {
                        Log.e(FileManagerImpl.class.getName(), e.getMessage());
                    }
                }
            }
        }
        return fileContentBuilder.toString();
    }
}
