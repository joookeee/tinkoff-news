package com.larkin.tinkoffnews.news;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.larkin.tinkoffnews.R;
import com.larkin.tinkoffnews.content.model.NewsPayload;

import java.util.Collections;
import java.util.List;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {
    private List<NewsPayload> newsList;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int id);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_title, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(newsList.get(position));
    }

    @Override
    public int getItemCount() {
        return newsList != null ? newsList.size() : 0;
    }

    public void swap(List<NewsPayload> news) {
        newsList = news;
        Collections.sort(newsList);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;

        ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            title = (TextView) view.findViewById(R.id.title);
        }

        void bind(NewsPayload payload) {
            title.setText(Html.fromHtml(payload.getText()));
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(newsList.get(getAdapterPosition()).getId());
        }
    }
}
