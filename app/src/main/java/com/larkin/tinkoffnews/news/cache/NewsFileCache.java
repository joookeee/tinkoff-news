package com.larkin.tinkoffnews.news.cache;

import com.google.gson.Gson;
import com.larkin.tinkoffnews.content.model.News;
import com.larkin.tinkoffnews.source.local.Cache;
import com.larkin.tinkoffnews.source.local.FileManager;

import java.io.File;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsFileCache implements Cache<News>{

    private final File cacheFile;
    private final FileManager fileManager;
    private final Gson gson = new Gson();

    public NewsFileCache(File cacheDir, FileManager fileManager) {
        cacheFile = new File(cacheDir, "news.json");
        this.fileManager = fileManager;
    }

    @Override
    public News get() {
        String json = fileManager.readFromFile(cacheFile);
        return gson.fromJson(json, News.class);
    }

    @Override
    public void put(News news) {
        fileManager.writeToFile(cacheFile, gson.toJson(news));
    }

    @Override
    public boolean delete() {
        return cacheFile.delete();
    }

    @Override
    public boolean isEmpty() {
        return !cacheFile.exists();
    }
}
