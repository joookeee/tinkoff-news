package com.larkin.tinkoffnews.news.interfaces;

import com.larkin.tinkoffnews.base.BaseView;
import com.larkin.tinkoffnews.content.model.Content;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NewsDetailsView extends BaseView {
    void showNewsDetails(Content content);
}
