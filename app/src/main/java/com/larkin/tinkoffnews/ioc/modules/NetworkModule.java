package com.larkin.tinkoffnews.ioc.modules;

import android.content.Context;

import com.larkin.tinkoffnews.network.NetworkManager;
import com.larkin.tinkoffnews.utils.NetworkManagerImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dlarkin on 19.03.2017.
 */

@Module
public class NetworkModule {

    @Provides
    NetworkManager provideNetworkManager(Context appContext) {
        return new NetworkManagerImpl(appContext);
    }
}
