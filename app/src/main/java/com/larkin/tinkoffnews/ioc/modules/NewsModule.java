package com.larkin.tinkoffnews.ioc.modules;

import com.larkin.tinkoffnews.content.repository.NewsRepository;
import com.larkin.tinkoffnews.network.NetworkManager;
import com.larkin.tinkoffnews.news.NewsDetailsPresenterImpl;
import com.larkin.tinkoffnews.news.NewsListPresenterImpl;
import com.larkin.tinkoffnews.news.interfaces.NewsDetailsPresenter;
import com.larkin.tinkoffnews.news.interfaces.NewsListPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dlarkin on 19.03.2017.
 */

@Module
public class NewsModule {

    @Provides
    NewsListPresenter provideNewsListPresenter(@Named("coordinator") NewsRepository newsRepository, NetworkManager networkManager) {
        return new NewsListPresenterImpl(newsRepository, networkManager);
    }

    @Provides
    NewsDetailsPresenter provideNewsDetailsPresenter(@Named("coordinator") NewsRepository newsRepository, NetworkManager networkManager) {
        return new NewsDetailsPresenterImpl(newsRepository, networkManager);
    }
}
