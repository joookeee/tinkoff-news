package com.larkin.tinkoffnews.ioc.modules;

import android.content.Context;

import com.larkin.tinkoffnews.content.model.News;
import com.larkin.tinkoffnews.content.repository.NewsRepository;
import com.larkin.tinkoffnews.news.cache.FileManagerImpl;
import com.larkin.tinkoffnews.news.cache.NewsFileCache;
import com.larkin.tinkoffnews.source.NewsRepositoryImpl;
import com.larkin.tinkoffnews.source.local.Cache;
import com.larkin.tinkoffnews.source.remote.RemoteNewsRepository;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dlarkin on 19.03.2017.
 */

@Module
public class RepositoryModule {

    @Provides
    @Named("remote")
    NewsRepository provideRemoteRepository() {
        return new RemoteNewsRepository();
    }

    @Provides
    Cache<News> provideNewsCache(Context appContext) {
        return new NewsFileCache(appContext.getCacheDir(), new FileManagerImpl());
    }

    @Provides
    @Named("coordinator")
    NewsRepository provideNewsRepository(Cache<News> newsCache, @Named("remote") NewsRepository remoteRepo) {
        return new NewsRepositoryImpl(newsCache, remoteRepo);
    }
}
