package com.larkin.tinkoffnews.ioc;

import com.larkin.tinkoffnews.ioc.modules.ApplicationModule;
import com.larkin.tinkoffnews.ioc.modules.NetworkModule;
import com.larkin.tinkoffnews.ioc.modules.NewsModule;
import com.larkin.tinkoffnews.ioc.modules.RepositoryModule;
import com.larkin.tinkoffnews.news.NewsDetailsFragment;
import com.larkin.tinkoffnews.news.NewsListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by dlarkin on 19.03.2017.
 */
@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                RepositoryModule.class,
                NetworkModule.class,
                NewsModule.class
        }
)
public interface IocComponent {
    void inject(NewsListFragment fragment);
    void inject(NewsDetailsFragment fragment);
}
