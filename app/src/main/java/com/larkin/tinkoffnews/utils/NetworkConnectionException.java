package com.larkin.tinkoffnews.utils;

/**
 * Created by dlarkin on 19.03.2017.
 */
public class NetworkConnectionException extends Exception {

    public NetworkConnectionException() {
        super();
    }

    public NetworkConnectionException(String message) {
        super(message);
    }

    public NetworkConnectionException(Throwable cause) {
        super(cause);
    }
}
