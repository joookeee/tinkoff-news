package com.larkin.tinkoffnews.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.larkin.tinkoffnews.network.NetworkManager;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NetworkManagerImpl implements NetworkManager{
    private ConnectivityManager connectivityManager;

    public NetworkManagerImpl(Context context) {
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public boolean isNetworkAvailable() {
        NetworkInfo activeInfo = connectivityManager.getActiveNetworkInfo();
        return (activeInfo != null && activeInfo.isConnected());
    }
}
