package com.larkin.tinkoffnews.utils.interfaces;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NewsNavigationController {
    void openNews(int id);
}
