package com.larkin.tinkoffnews.network.api;

import com.larkin.tinkoffnews.content.model.Content;
import com.larkin.tinkoffnews.content.model.News;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NewsApi {
    @GET("news")
    Observable<News> news();

    @GET("news_content")
    Observable<Content> content(@Query("id") int id);
}
