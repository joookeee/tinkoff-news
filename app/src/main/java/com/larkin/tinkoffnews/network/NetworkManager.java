package com.larkin.tinkoffnews.network;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface NetworkManager {
    boolean isNetworkAvailable();
}
