package com.larkin.tinkoffnews;

import android.app.Application;

import com.larkin.tinkoffnews.ioc.DaggerIocComponent;
import com.larkin.tinkoffnews.ioc.IocComponent;
import com.larkin.tinkoffnews.ioc.modules.ApplicationModule;
import com.larkin.tinkoffnews.ioc.modules.NetworkModule;
import com.larkin.tinkoffnews.ioc.modules.NewsModule;
import com.larkin.tinkoffnews.ioc.modules.RepositoryModule;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class TinkoffNewsApplication extends Application {

    private static IocComponent iocComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        iocComponent = buildDaggerIocComponent();
    }

    public static IocComponent getIocComponent() {
        return iocComponent;
    }

    private IocComponent buildDaggerIocComponent() {
        return DaggerIocComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .repositoryModule(new RepositoryModule())
                .networkModule(new NetworkModule())
                .newsModule(new NewsModule())
                .build();
    }
}
