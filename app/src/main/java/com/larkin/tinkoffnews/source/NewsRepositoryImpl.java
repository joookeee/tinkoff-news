package com.larkin.tinkoffnews.source;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.larkin.tinkoffnews.content.model.Content;
import com.larkin.tinkoffnews.content.model.News;
import com.larkin.tinkoffnews.content.repository.NewsRepository;
import com.larkin.tinkoffnews.source.local.Cache;

import rx.Observable;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class NewsRepositoryImpl implements NewsRepository {

    private NewsRepository remoteRepository;
    private Cache<News> newsCache;
    private SparseArray<Content> cachedContent;

    public NewsRepositoryImpl(Cache<News> newsCache, NewsRepository repoteRepo) {
        this.remoteRepository = repoteRepo;
        this.newsCache = newsCache;
        cachedContent = new SparseArray<>();
    }

    @Override
    public Observable<News> getNews() {
        if (newsCache.isEmpty()) {
            return getNewsFromRemote();
        }
        return remoteRepository.getNews()
                .onErrorResumeNext(getNewsFromCache())
                .filter(news -> !news.equals(newsCache.get()))
                .doOnNext(news -> newsCache.put(news))
                .switchIfEmpty(getNewsFromCache());
    }

    @Override
    public Observable<Content> getNewsDetails(int id) {
        Content newsContent = cachedContent.get(id);
        if (newsContent != null) {
            return Observable.just(newsContent);
        }

        return remoteRepository.getNewsDetails(id)
                .doOnNext(content -> cachedContent.put(id, content));
    }

    @NonNull
    private Observable<News> getNewsFromRemote() {
        return remoteRepository.getNews().doOnNext(news -> newsCache.put(news));
    }

    @NonNull
    private Observable<News> getNewsFromCache() {
        return Observable.just(newsCache.get());
    }
}
