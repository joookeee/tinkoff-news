package com.larkin.tinkoffnews.source.local;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface Cache<T> {
    T get();

    void put(T object);

    boolean delete();

    boolean isEmpty();
}
