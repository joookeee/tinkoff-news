package com.larkin.tinkoffnews.source.remote;

import com.larkin.tinkoffnews.content.model.Content;
import com.larkin.tinkoffnews.content.model.News;
import com.larkin.tinkoffnews.content.repository.NewsRepository;
import com.larkin.tinkoffnews.network.api.NewsApi;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by dlarkin on 19.03.2017.
 */

public class RemoteNewsRepository implements NewsRepository{

    private static final String ENDPOINT = "https://api.tinkoff.ru/v1/";
    private NewsApi api;

    public RemoteNewsRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        api = retrofit.create(NewsApi.class);
    }

    @Override
    public Observable<News> getNews() {
        return api.news();
    }

    @Override
    public Observable<Content> getNewsDetails(int id) {
        return api.content(id);
    }
}
