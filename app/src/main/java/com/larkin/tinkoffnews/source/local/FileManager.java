package com.larkin.tinkoffnews.source.local;

import java.io.File;

/**
 * Created by dlarkin on 19.03.2017.
 */

public interface FileManager {

    void writeToFile(File file, String content);

    String readFromFile(File file);
}
